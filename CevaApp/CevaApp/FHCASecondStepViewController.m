//
//  FHCASecondStepViewController.m
//  CevaApp
//
//  Created by Fabio Barboza on 1/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHCASecondStepViewController.h"

@interface FHCASecondStepViewController ()
@property (weak, nonatomic) IBOutlet UIPickerView *pkCurrentTemperature;
@property (weak, nonatomic) IBOutlet UIPickerView *pkDesiredTemperature;
@property (nonatomic) NSMutableArray *temperatureList;

@end

@implementation FHCASecondStepViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _pkCurrentTemperature.delegate = self;
    _pkCurrentTemperature.dataSource = self;
    _pkDesiredTemperature.delegate = self;
    _pkDesiredTemperature.dataSource = self;
    [_pkCurrentTemperature setTintColor:[UIColor whiteColor]];
    [_pkDesiredTemperature setTintColor:[UIColor whiteColor]];
    [self createBottleList];
    super.view.backgroundColor = [UIColor colorWithRed:255.0f/255.0f
                                                 green:99.0f/255.0f
                                                  blue:71.0f/255.0f
                                                 alpha:1];
    [self createComponents];
}

- (void)createComponents
{
    UIButton *nextPage = [[UIButton alloc] initWithFrame:CGRectMake(60, 450, 200, 40)];
    [nextPage setTitle:@"Proximo Passo!" forState:UIControlStateNormal];
    [nextPage setTitleColor:[UIColor colorWithRed:255.0f/255.0f
                                            green:99.0f/255.0f
                                             blue:71.0f/255.0f
                                            alpha:1] forState:UIControlStateNormal];
    nextPage.backgroundColor = [UIColor whiteColor];
    [[nextPage layer] setBorderWidth:1.0f];
    nextPage.layer.cornerRadius = 15.0;
    [[nextPage layer] setBorderColor:[UIColor whiteColor].CGColor];
    [nextPage addTarget:self action:@selector(nextPageAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextPage];
}

- (IBAction)nextPageAction:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"thirdStepSegue" sender:nil];
}

- (void)createBottleList
{
    _temperatureList = [[NSMutableArray alloc] init];
    [_temperatureList addObject:@"Muito quente"];
    [_temperatureList addObject:@"Quente"];
    [_temperatureList addObject:@"Temperatura Ambiente"];
    [_temperatureList addObject:@"Gelada"];
    [_temperatureList addObject:@"Muito gelada"];
}

//- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
//{
//    return 200.0;
//}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return (NSString *)_temperatureList[row];
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _temperatureList.count;
}

//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
//{
//    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
//    viewHeader.backgroundColor = [UIColor colorWithRed:255.0f/255.0f
//                                                 green:99.0f/255.0f
//                                                  blue:71.0f/255.0f
//                                                 alpha:1];
//    
//    
//    FHCABottle *aux = (FHCABottle *)_bottleList[row];
//    
//    UILabel * label = [[UILabel alloc] init];
//    label.frame = CGRectMake(0, 90, 200, 30);
//    label.font = [UIFont systemFontOfSize:30.0f];
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor whiteColor];
//    label.text = aux.bottleType;
//    [viewHeader addSubview: label];
//    
//    UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectMake(130, 0, 140, 189)];
//    UIImage *image = aux.image;
//    imgview.image = image;
//    [viewHeader addSubview:imgview];
//    
//    return viewHeader;
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
