//
//  FHCASimpleStartViewController.m
//  CevaApp
//
//  Created by Fabio Barboza on 1/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHCASimpleStartViewController.h"
#import "FHCABottle.h"

@interface FHCASimpleStartViewController ()
@property (weak, nonatomic) IBOutlet UIPickerView *pkBottleSize;
@property (nonatomic) NSMutableArray *bottleList;

@end

@implementation FHCASimpleStartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Primeiro Passo!";
    _pkBottleSize.delegate = self;
    _pkBottleSize.dataSource = self;
    [self createBottleList];
    super.view.backgroundColor = [UIColor colorWithRed:255.0f/255.0f
                                                 green:99.0f/255.0f
                                                  blue:71.0f/255.0f
                                                 alpha:1];
    [self createComponents];
}

- (void)createComponents
{
    UIButton *nextPage = [[UIButton alloc] initWithFrame:CGRectMake(60, 450, 200, 40)];
    [nextPage setTitle:@"Proximo Passo!" forState:UIControlStateNormal];
    [nextPage setTitleColor:[UIColor colorWithRed:255.0f/255.0f
                                            green:99.0f/255.0f
                                             blue:71.0f/255.0f
                                            alpha:1] forState:UIControlStateNormal];
    nextPage.backgroundColor = [UIColor whiteColor];
    [[nextPage layer] setBorderWidth:1.0f];
    nextPage.layer.cornerRadius = 15.0;
    [[nextPage layer] setBorderColor:[UIColor whiteColor].CGColor];
    [nextPage addTarget:self action:@selector(nextPageAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextPage];
}

- (IBAction)nextPageAction:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"secondStepSegue" sender:nil];
}

- (void)createBottleList
{
    _bottleList = [[NSMutableArray alloc] init];
    
    UIImage *image1 = [UIImage imageNamed:@"bigBeer.png"];
    FHCABottle *aux1 = [[FHCABottle alloc] initWithCapacity:1000 andBottleType:@"Litrão" andImage:image1];
    [_bottleList addObject:aux1];
    
    UIImage *image2 = [UIImage imageNamed:@"longNeckBeer.png"];
    FHCABottle *aux2 = [[FHCABottle alloc] initWithCapacity:600 andBottleType:@"Garrafa" andImage:image2];
    [_bottleList addObject:aux2];
    
//    UIImage *image3 = [UIImage imageNamed:@"longneck.png"];
//    FHCABottle *aux3 = [[FHCABottle alloc] initWithCapacity:400 andBottleType:@"Long Neck" andImage:image3];
//    [_bottleList addObject:aux3];
    
//    UIImage *image4 = [UIImage imageNamed:@"latao.png"];
//    FHCABottle *aux4 = [[FHCABottle alloc] initWithCapacity:500 andBottleType:@"Latão" andImage:image4];
//    [_bottleList addObject:aux4];
    
    UIImage *image5 = [UIImage imageNamed:@"smallBeer.png"];
    FHCABottle *aux5 = [[FHCABottle alloc] initWithCapacity:375 andBottleType:@"Lata" andImage:image5];
    [_bottleList addObject:aux5];
    
//    UIImage *image6 = [UIImage imageNamed:@"latinha.png"];
//    FHCABottle *aux6 = [[FHCABottle alloc] initWithCapacity:295 andBottleType:@"Latinha" andImage:image6];
//    [_bottleList addObject:aux6];
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _bottleList.count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
    viewHeader.backgroundColor = [UIColor colorWithRed:255.0f/255.0f
                                                 green:99.0f/255.0f
                                                  blue:71.0f/255.0f
                                                 alpha:1];

    
    FHCABottle *aux = (FHCABottle *)_bottleList[row];
    
    UILabel * label = [[UILabel alloc] init];
    label.frame = CGRectMake(0, 90, 200, 30);
    label.font = [UIFont systemFontOfSize:30.0f];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = aux.bottleType;
    [viewHeader addSubview: label];
    
    UIImageView *imgview = [[UIImageView alloc] initWithFrame:CGRectMake(130, 0, 140, 189)];
    UIImage *image = aux.image;
    imgview.image = image;
    [viewHeader addSubview:imgview];

    return viewHeader;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 200.0;
}
//
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    return (NSString *)_bottleList[row];
//}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
