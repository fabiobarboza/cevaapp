//
//  FHCAThirdStepViewController.m
//  CevaApp
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHCAThirdStepViewController.h"

@interface FHCAThirdStepViewController ()
@property (nonatomic) UIImageView *imgView;
@property UIImage *image1;
@property UIImage *image2;
@property UIImage *image3;
@property UIImage *image4;
@property UIImage *image5;
@property (nonatomic) int temp;

@end

@implementation FHCAThirdStepViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _image1 = [UIImage imageNamed:@"temp1.png"];
    _image2 = [UIImage imageNamed:@"temp2.png"];
    _image3 = [UIImage imageNamed:@"temp3.png"];
    _image4 = [UIImage imageNamed:@"temp4.png"];
    _image5 = [UIImage imageNamed:@"temp5.png"];
    _temp = 3;
    _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(60, 150, 200, 200)];
    _imgView.image = _image3;
    [self.view addSubview:_imgView];
    super.view.backgroundColor = [UIColor colorWithRed:255.0f/255.0f
                                                 green:99.0f/255.0f
                                                  blue:71.0f/255.0f
                                                 alpha:1];
    [self createComponents];

}


- (void)createComponents
{
    UIButton *btnUp = [[UIButton alloc] initWithFrame:CGRectMake(210, 450, 50, 40)];
    [btnUp setTitle:@"MAIS!" forState:UIControlStateNormal];
    [btnUp setTitleColor:[UIColor colorWithRed:255.0f/255.0f
                                            green:99.0f/255.0f
                                             blue:71.0f/255.0f
                                            alpha:1] forState:UIControlStateNormal];
    btnUp.backgroundColor = [UIColor whiteColor];
    [[btnUp layer] setBorderWidth:1.0f];
    btnUp.layer.cornerRadius = 15.0;
    [[btnUp layer] setBorderColor:[UIColor whiteColor].CGColor];
    [btnUp addTarget:self action:@selector(down:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnUp];
    
    UIButton *btnDown = [[UIButton alloc] initWithFrame:CGRectMake(60, 450, 50, 40)];
    [btnDown setTitle:@"MENOS!" forState:UIControlStateNormal];
    [btnDown setTitleColor:[UIColor colorWithRed:255.0f/255.0f
                                            green:99.0f/255.0f
                                             blue:71.0f/255.0f
                                            alpha:1] forState:UIControlStateNormal];
    btnDown.backgroundColor = [UIColor whiteColor];
    [[btnDown layer] setBorderWidth:1.0f];
    btnDown.layer.cornerRadius = 15.0;
    [[btnDown layer] setBorderColor:[UIColor whiteColor].CGColor];
    [btnDown addTarget:self action:@selector(up:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDown];
}

- (IBAction)up:(UIButton *)sender
{
    switch (_temp)
    {
        case 1: _imgView.image = _image2;
            _temp ++;
            break;
        case 2: _imgView.image = _image3;
            _temp ++;
            break;
        case 3: _imgView.image = _image4;
            _temp ++;
            break;
        case 4: _imgView.image = _image5;
            _temp ++;
            break;
        case 5: _imgView.image = _image5;
            break;
    }

}

- (IBAction)down:(UIButton *)sender
{
    switch (_temp)
    {
        case 1: _imgView.image = _image1;
            break;
        case 2: _imgView.image = _image1;
            _temp --;
            break;
        case 3: _imgView.image = _image2;
            _temp --;
            break;
        case 4: _imgView.image = _image3;
            _temp --;
            break;
        case 5: _imgView.image = _image4;
            _temp --;
            break;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
