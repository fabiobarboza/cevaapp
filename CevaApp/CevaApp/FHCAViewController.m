//
//  FHCAViewController.m
//  CevaApp
//
//  Created by Fabio Barboza on 1/16/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHCAViewController.h"

@interface FHCAViewController ()

@end

@implementation FHCAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self createScrollMenu];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createScrollMenu
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 100)];
    
    int x = 0;
    UIButton *button;
    for (int i = 0; i < 8; i++) {
        
        
        button = [[UIButton alloc] initWithFrame:CGRectMake(x, 100, 100, 100)];
        [button setTitle:@"jidsjaioa" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTintColor:[UIColor greenColor]];
        button.backgroundColor = [UIColor blueColor];
        [scrollView addSubview:button];
        
        x += button.frame.size.width + 10;
    }
    
    scrollView.contentSize = CGSizeMake(x, scrollView.frame.size.height);
    scrollView.backgroundColor = [UIColor grayColor];

    [self.view addSubview:scrollView];
}

- (void)createComponents
{
//    UIButton *btnSimpleTask = [[UIButton alloc] initWithFrame:CGRectMake(60, 200, 200, 50)];
//    [btnSimpleTask setTitle:@"Inicialização Rápida" forState:UIControlStateNormal];
//    [btnSimpleTask setTitleColor:self.view.tintColor forState:UIControlStateNormal];
//    [btnSimpleTask addTarget:self action:@selector(simpleStart:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btnSimpleTask];
//    
//    
//    UIButton *btnCompleteTask = [[UIButton alloc] initWithFrame:CGRectMake(60, 250, 200, 50)];
//    [btnCompleteTask setTitle:@"Inicialização Avançada" forState:UIControlStateNormal];
//    [btnCompleteTask setTitleColor:self.view.tintColor forState:UIControlStateNormal];
//    [btnCompleteTask addTarget:self action:@selector(completeStart:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btnCompleteTask];

}

-(IBAction)simpleStart:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"simpleStartSegue" sender:nil];
}

-(IBAction)completeStart:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"completeStartSegue" sender:nil];
}

@end
