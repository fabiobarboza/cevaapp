//
//  FHCAAppDelegate.h
//  CevaApp
//
//  Created by Fabio Barboza on 1/16/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHCAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
