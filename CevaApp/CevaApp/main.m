//
//  main.m
//  CevaApp
//
//  Created by Fabio Barboza on 1/16/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FHCAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FHCAAppDelegate class]));
    }
}
