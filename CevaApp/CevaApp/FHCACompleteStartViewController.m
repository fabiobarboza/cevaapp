//
//  FHCACompleteStartViewController.m
//  CevaApp
//
//  Created by Fabio Barboza on 1/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHCACompleteStartViewController.h"

@interface FHCACompleteStartViewController ()

@end

@implementation FHCACompleteStartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
