//
//  FHCABottle.h
//  CevaApp
//
//  Created by Fabio Barboza on 1/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHCABottle : NSObject


@property (nonatomic) double capacity;
@property (nonatomic) NSString *bottleType;
@property (nonatomic) UIImage *image;
- (instancetype)initWithCapacity:(double)aCapacity andBottleType:(NSString *)aType andImage:(UIImage *)aImage;
@end
