//
//  FHCABottle.m
//  CevaApp
//
//  Created by Fabio Barboza on 1/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHCABottle.h"

@implementation FHCABottle

- (instancetype)init
{
    
    return [self initWithCapacity:0 andBottleType:@"" andImage:nil];
}


- (instancetype)initWithCapacity:(double)aCapacity andBottleType:(NSString *)aType andImage:(UIImage *)aImage
{
    self = [super init];
    if (self)
    {
        _capacity = aCapacity;
        _bottleType = aType;
        _image = aImage;
    }
    return self;
}


@end
