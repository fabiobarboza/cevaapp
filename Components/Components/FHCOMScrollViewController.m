//
//  FHCOMScrollViewController.m
//  Components
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHCOMScrollViewController.h"

@interface FHCOMScrollViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation FHCOMScrollViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    int currY = 0;
    for (int i = 0; i < 100; i++)
    {
        UILabel *aux = [[UILabel alloc] initWithFrame:CGRectMake(60, currY, 200, 40)];
        currY += aux.frame.size.height;
        aux.text = [NSString stringWithFormat:@"Produto %d", i+1];
        [self.scrollView addSubview:aux];
    }
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, currY)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
