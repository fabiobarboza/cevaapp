//
//  FHCOMViewController.m
//  Components
//
//  Created by Fabio Barboza on 1/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHCOMViewController.h"

@interface FHCOMViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UISwitch *myswitch;
@property (nonatomic) BOOL permitido;

@end

@implementation FHCOMViewController

-(void)setPermitido:(BOOL)permitido
{
    _permitido = permitido;
    if (permitido)
        self.label.text = @"YES";
    else
        self.label.text = @"NO";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTouchSwitch:(UISwitch *)sender {
    NSLog(@"Switch: %d", sender.on);
    self.permitido = sender.on;
}

- (IBAction)didTouchButton:(UIButton *)sender {
    [_myswitch setOn:!_myswitch.on animated:YES];
}

@end
